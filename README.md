# Coursepublisher

A simple application to publish Open edX courses on MeinBildungsraum.

## Configuration

Copy `coursepublisher/apikeys.py.example` to `coursepublisher/apikeys.py`, and enter your client ID and secret for the MeinBildungsraum API.

TODO:
- How to configure the address of edX
- How to deploy
- How to use the tool
