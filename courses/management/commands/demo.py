
from django.conf import settings
from django.core.management.base import BaseCommand
from courses.bildungsraum import PushApi
from courses.models import BildungsraumCourse, update_or_create2

class Command(BaseCommand):
    help = 'Debugging command'

    def handle(self, *args, **options):
        api = PushApi(
            settings.BILDUNGSRAUM_API_BASE,
            settings.BILDUNGSRAUM_CLIENT_ID,
            settings.BILDUNGSRAUM_CLIENT_SECRET,
            settings.BILDUNGSRAUM_TOKEN_URL
        )
        #api.datenraum_about()
        #print(api.access_token)

        nodes = api.get_nodes('OpenJupyter')
        for node in nodes:
            obj, created, updated = update_or_create2(
                BildungsraumCourse,
                externalId=node.externalId,
                defaults={
                    'title': node.title,
                    'description': node.description,
                    'sourceId': node.sourceId,
                    'metadata': node.metadata,
                    'nodeId': node.id,
                }
            )

            # print with color using django shortcuts
            if created:
                self.stdout.write(self.style.SUCCESS(f'Created {obj}'))
            elif updated:
                self.stdout.write(self.style.SUCCESS(f'Updated {obj}'))
            else:
                self.stdout.write(f'No changes {obj}')
            

