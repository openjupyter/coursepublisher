
from django.conf import settings
from django.core.management.base import BaseCommand
from courses.bildungsraum import PushApi


class Command(BaseCommand):
    help = 'Returns a valid access token for the Datenraum API'

    def handle(self, *args, **options):
        api = PushApi(
            settings.BILDUNGSRAUM_API_BASE,
            settings.BILDUNGSRAUM_CLIENT_ID,
            settings.BILDUNGSRAUM_CLIENT_SECRET,
            settings.BILDUNGSRAUM_TOKEN_URL
        )
        api.datenraum_about()
        print(api.access_token)
