
from __future__ import annotations

import pickle
import time
from datetime import datetime
from typing import Optional, TypedDict, Any, Iterator
from .types import Node, NodeList, PaginatedResponse

import httpx

CACHEFILE = 'cache.pickle'

def get_from_cache(key: str):
    """ Returns a value from the cache, or None if the key is not found or it is expired. """
    try:
        with open(CACHEFILE, 'rb') as f:
            cache = pickle.load(f)
        if key in cache:
            value, expires = cache[key]
            if expires is None or expires > time.time():
                return value
    except FileNotFoundError:
        pass
    return None

def set_to_cache(key: str, value: Any, ttl: Optional[int]=None):
    """ Sets a value in the cache with a time-to-live (ttl) in seconds. """
    try:
        with open(CACHEFILE, 'rb') as f:
            cache = pickle.load(f)
    except FileNotFoundError:
        cache = {}
    if ttl is None:
        cache[key] = (value, None)
    else:
        cache[key] = (value, time.time() + ttl)
    with open(CACHEFILE, 'wb') as f:
        pickle.dump(cache, f)


COURSE_CREATED = 201
COURSE_UPDATED = 204


class PushApiError(RuntimeError):
    pass


class UnauthorizedError(PushApiError):
    pass


class CourseNotFoundError(PushApiError):
    def __init__(self, courseId: str):
        super().__init__(f"Course {courseId} not found")

        
class PushApi:
    def __init__(self, api_base: str, client_id: str, client_secret: str, token_url: str):
        self.api_base = api_base
        self.client_id = client_id
        self.client_secret = client_secret
        self.token_url = token_url
        headers = {
            'Accept': 'application/json',
        }
        auth = ApiTokenAuth(token_url, client_id, client_secret)
        self._client = httpx.Client(headers=headers, auth=auth)


    @property
    def access_token(self) -> str:
        return self._client.auth.access_token


    sourceSlug = 'OpenJupyter'
    def get_course_ids(self):
        url = f'{self.api_base}/datenraum/api/core/nodes'
        params = {
            'sourceSlug': 'OpenJupyter',
            'offset': '0',
            'limit': '10',
        }
        response = self._client.get(url, params=params)
        response.raise_for_status()
        obj = response.json()
        for node in obj['_embedded']['nodes']:
            yield node['externalId']


    def get_nodes(self, sourceSlug: str) -> Iterator[Node]:
        url = f'{self.api_base}/datenraum/api/core/nodes'
        offset = 0
        limit = 2
        params = {
            'sourceSlug': sourceSlug,
            'offset': str(offset),
            'limit': str(limit),
        }
        response = self._client.get(url, params=params)
        response.raise_for_status()
        while True:
            pr = PaginatedResponse[NodeList].model_validate_json(response.text)
            for node in pr.embedded.nodes:
                yield node
            if pr.links.next is None:
                break
            response = self._client.get(pr.links.next.href)
            response.raise_for_status()


    def publish_course(self, courseId: str, title: str, description: str, uri: str, startDate: Optional[datetime]=None, endDate: Optional[datetime]=None, cost: Optional[float]=None) -> int:
        url = f'{self.api_base}/push-connector/api/course/{self.sourceSlug}/{courseId}'
        data = {
            "title": title,
            "description": description,
            "uri": uri,
            "cost": 1234.56,
            # "address": "Schanzenstraße 75-77, 20357 Hamburg",
            # "latitude": "53.5621820995914",
            # "longitude": "9.965724480663814",
            "startDate": try_format_date(startDate),
            "endDate": try_format_date(endDate),
            "cost": cost,
        }
        print("PUT to url", url, data)
        response = self._client.put(url, json=data)
        print("Response", response.status_code, response.text)
        response.raise_for_status()
        return response.status_code


    def get_course(self, courseId: str) -> GetCourseResponse:
        url = f'{self.api_base}/push-connector/api/course/{self.sourceSlug}/{courseId}'
        response = self._client.get(url)
        response.raise_for_status()
        return response.json()

    
    def unpublish_course(self, courseId: str):
        url = f'{self.api_base}/push-connector/api/course/{self.sourceSlug}/{courseId}'
        response = self._client.delete(url)
        if response.status_code == 404:
            raise CourseNotFoundError(courseId)
        response.raise_for_status()


    def get_node_by_external_id(self, sourceId: str, externalId: str, includeRelatedNodes: Optional[bool]=None):
        url = f'{self.api_base}/datenraum/api/core/nodes'
        params = {
            'sourceId': sourceId,
            'externalId': externalId,
        }
        if includeRelatedNodes is not None:
            params['includeRelatedNodes'] = 'true' if includeRelatedNodes else 'false'
        response = self._client.get(url, params=params)
        print(response.url)
        response.raise_for_status()
        return response.json()
    

    # TODO: support x-health-check-api-key header?
    def datenraum_about(self):
        url = f'{self.api_base}/datenraum/about'
        response = self._client.get(url)
        response.raise_for_status()
        return response.json()
    

class ApiTokenAuth(httpx.Auth):
    """ Custom auth that gets a new token when required. """
    requires_response_body = True

    def __init__(self, token_url: str, client_id: str, client_secret: str):
        self.access_token = get_from_cache('token')
        self.token_url = token_url
        self.client_id = client_id
        self.client_secret = client_secret


    def auth_flow(self, request):
        if not self.access_token:
            # Fetch a new token
            print("No token, fetching one")
            refresh_response = yield self.build_refresh_request()
            self.extract_token(refresh_response)

        # try the request
        request.headers['Authorization'] = 'Bearer ' + self.access_token
        response = yield request
        
        if response.status_code == 401:
            # Fetch a new token
            print("Token expired, fetching a new one")
            refresh_response = yield self.build_refresh_request()
            self.extract_token(refresh_response)
        
            # Retry the request
            assert self.access_token is not None
            request.headers['Authorization'] = 'Bearer ' + self.access_token        
            yield request


    def build_refresh_request(self) -> httpx.Request:
        """ Builds a request to get a new token. """
        data = { 'grant_type': 'client_credentials' }
        request = httpx.Request(method='POST', url=self.token_url, data=data)
        basic_auth = httpx.BasicAuth(self.client_id, self.client_secret)
        request.headers["Authorization"] = basic_auth._auth_header
        return request


    def extract_token(self, response: httpx.Response):
        """ Extracts the token from the response and saves it. """
        res = response.json()
        token = res['access_token']
        expires_in = res['expires_in']
        set_to_cache('token', token, expires_in)
        self.access_token = token


def try_format_date(date: Optional[datetime]) -> Optional[str]:
    if date is None:
        return None
    # if date has no timezone, assume it is UTC
    if date.tzinfo is None:
        date = date.astimezone()
    return date.isoformat()

class AMBDict(TypedDict):
    id: str
    name: str
    type: list[str]
    offers: list[dict]
    publisher: list[dict]
    description: str
    hasCourseInstance: dict
    isAccessibleForFree: bool

class GetCourseResponse(TypedDict):
    organization: str
    url: str
    amb: AMBDict
