import json
from django.contrib import admin
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter
from django.utils.safestring import mark_safe
from django.contrib import admin
from django.contrib import messages
from django.http import HttpRequest, HttpResponseRedirect
from admin_extra_buttons.api import ExtraButtonsMixin, button
from courses.bildungsraum import CourseNotFoundError
import jsbeautifier
from typing import Any

# Register your models here.
from .models import EdxCourse, BildungsraumCourse, refresh_edx_courses, refresh_bildungsraum_courses

class EdxCourseAdmin(ExtraButtonsMixin, admin.ModelAdmin):
    change_form_template = 'admin/custom_change_form.html'
    list_display = ('name', 'number', 'org', 'short_description', 'is_published')
    readonly_fields = (
        'name', 'number', 'org', 'course_id', 'short_description', 'bildungsraum_course', 'studio_link',
        'start_date', 'end_date', 'price_euro', 'is_published', 'course_url'
    )
    fields = ('name', 'course_id', 'short_description', 'bildungsraum_course', 'course_url', 'studio_link', ('start_date', 'end_date'), 'is_published')
    actions = ['publish_selected', 'unpublish_selected']

    def has_add_permission(self, request: HttpRequest) -> bool:
        return False
    
    def has_delete_permission(self, request: HttpRequest, obj: Any | None = ...) -> bool:
        return False

    def studio_url(self, obj: EdxCourse):
        return f"https://studio.edx.open-jupyter.gwdg.de/course/{obj.course_id}"

    def studio_link(self, obj: EdxCourse):
        return mark_safe(f'<a href="{self.studio_url(obj)}" target="_blank">{obj.name}</a>')

    # Publish multiple selected courses
    def publish_selected(self, request, queryset):
        for course in queryset:
            course.publish_to_bildungsraum()
        messages.info(request, f"Published {len(queryset)} courses to Bildungsraum")

    # Unpublish multiple selected courses
    def unpublish_selected(self, request, queryset):
        for course in queryset:
            if not course.bildungsraum_course:
                continue

            try:
                course.unpublish_from_bildungsraum()
            except CourseNotFoundError:
                pass
        messages.info(request, f"Unpublished {len(queryset)} courses from Bildungsraum")

    @button()
    def refresh_courses(self, request):
        refresh_edx_courses(request)
        refresh_bildungsraum_courses(request)
        messages.info(request, "Courses refreshed")

    # make the start/stop buttons disabled as appropriate
    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        obj: EdxCourse = self.get_object(request, object_id)
        # extra_context['can_start_dashboard'] = not obj.container_running()
        extra_context['can_unpublish'] = obj.bildungsraum_course is not None
        # Can publish when the object is saved
        # Is this object already saved?
        extra_context['can_publish'] = obj.pk is not None
        # Do we "update" or "publish" the course?
        extra_context['already_published'] = obj.bildungsraum_course is not None
        return super().change_view(request, object_id, form_url, extra_context=extra_context)


    def response_change(self, request, obj: EdxCourse):
        if "_publish_to_bildungsraum" in request.POST:
            obj.publish_to_bildungsraum()
            messages.info(request, f"Publishing course {obj.name} to Bildungsraum")
            return HttpResponseRedirect(".")
        if "_unpublish_from_bildungsraum" in request.POST:
            obj.unpublish_from_bildungsraum()
            messages.info(request, f"Unpublishing course {obj.name} from Bildungsraum")
            return HttpResponseRedirect(".")
        # print("response_change", request, obj)
        # print(request.POST)
        # if "_build_dashboard" in request.POST:
        #     start_build_dashboard(obj.id)
        #     messages.info(request, f"Building dashboard {obj.name}")
        #     return HttpResponseRedirect(".")
        # # Start and stop should be fast enough to run without a task:
        # if "_start_dashboard" in request.POST:
        #     obj.run_container()
        #     return HttpResponseRedirect(".")
        # if "_stop_dashboard" in request.POST:
        #     obj.stop_container()
        #     return HttpResponseRedirect(".")
        return super().response_change(request, obj)

admin.site.register(EdxCourse, EdxCourseAdmin)

class BildungsraumCourseAdmin(ExtraButtonsMixin, admin.ModelAdmin):
    list_display = ('title', 'description', 'externalId', 'exists_locally')
    readonly_fields = (
        'title', 'description', 'nodeId', 'externalId', 'sourceId', 'edx_course', 'pretty_json', 'exists_locally', 'lernpfadfinder_url'
    )
    fields = ('title', 'description', 'nodeId', 'externalId', 'sourceId', 'edx_course', 'pretty_json', 'exists_locally', 'lernpfadfinder_url')

    def has_add_permission(self, request: HttpRequest) -> bool:
        return False

    def has_change_permission(self, request: HttpRequest, obj: Any | None = ...) -> bool:
        return False
    
    def has_delete_permission(self, request: HttpRequest, obj: Any | None = ...) -> bool:
        return False

    # # Set title from "Change..." to "View..."
    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     extra_context = extra_context or {}
    #     extra_context['title'] = f"View {self.model._meta.verbose_name}"
    #     return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def lernpfadfinder_url(self, obj: BildungsraumCourse):
        return f"https://portal.demo.meinbildungsraum.de/web/guest/lernpfadfinder#/offers/{obj.nodeId}"
    
    # The direct link doesn't currently work, since the Lernpfadfinder detects links from another domain
    # and doesn't allow this.
    def lernpfadfinder_link(self, obj: BildungsraumCourse):
        return mark_safe(f'<a href="{self.lernpfadfinder_url(obj)}" target="_blank">{obj.title}</a>')

    @button()
    def refresh_courses(self, request):
        refresh_edx_courses(request)
        refresh_bildungsraum_courses(request)
        messages.info(request, "Courses refreshed")

    @admin.display(description='Metadata')
    def pretty_json(self, instance: BildungsraumCourse):
        """Function to display pretty version of our data"""

        # Convert the data to sorted, indented JSON
        # response = json.dumps(instance.metadata, sort_keys=True, indent=2) # <-- your field here
        response = json.dumps(instance.metadata)
        response = jsbeautifier.beautify(response)

        # Truncate the data. Alter as needed
        response = response[:5000]

        # Get the Pygments formatter
        formatter = HtmlFormatter(style='default')

        # Highlight the data
        response = highlight(response, JsonLexer(), formatter)

        # Get the stylesheet
        style = "<style>" + formatter.get_style_defs() + "</style>"

        # Safe the output
        return mark_safe(style + response)

admin.site.register(BildungsraumCourse, BildungsraumCourseAdmin)