""" Defines some types for the Datenraum API. """
from __future__ import annotations
from typing import Generic, List, Optional, TypeVar
from pydantic import BaseModel, Field


class Node(BaseModel):
    id: str
    title: str
    description: str
    externalId: str
    sourceId: str
    # TODO: maybe this is too strict
    metadata: dict  # Metadata#|dict
    class_: str = Field(alias='class')
    min: Optional[int] = None
    max: Optional[int] = None
    _embedded: Optional[dict]
    _links: NodeLinks


class Metadata(BaseModel):
    amb: Amb = Field(alias='Amb')
    url: str = Field(alias='Url')
    organization: str = Field(alias='Organization')


class Amb(BaseModel):
    id: str
    name: str
    type: List[str]
    offers: Optional[List[dict]] = None
    # TODO: handle JSON-LD properly
    context: List[str | dict] = Field(alias='@context')
    publisher: List[dict]
    description: str
    hasCourseInstance: dict
    isAccessibleForFree: bool
#     container_name = 'nodes'


class NodeLinks(BaseModel):
    self: Link
    headNodes: Optional[Link]
    tailNodes: Optional[Link]
    childNodes: Optional[Link]


class Link(BaseModel):
    href: str


class NodeList(BaseModel):
    nodes: List[Node]


T = TypeVar('T')

class PaginatedResponse(BaseModel, Generic[T]):
    total: int
    offset: int
    limit: int
    embedded: T = Field(alias='_embedded')
    links: PaginationLinks = Field(alias='_links')


class PaginationLinks(BaseModel):
    # These can be null or a string
    self: Optional[Link] = Field()
    next: Optional[Link] = Field()
