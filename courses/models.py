from __future__ import annotations
import httpx
from django.contrib.admin.decorators import display
from django.db import models
from django.conf import settings
from datetime import datetime

from courses.bildungsraum import COURSE_CREATED, COURSE_UPDATED, PushApi
from django.contrib import messages

# Create your models here.
class EdxCourse(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=100)
    org = models.CharField(max_length=100)
    course_id = models.CharField(max_length=100)
    short_description = models.TextField()
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    # TODO: for multiple prices, we need to use the datenraum API
    price_euro = models.FloatField(null=True, blank=True)
    def __str__(self):
        return self.name

    # If published, link to the BildungsraumCourse:
    bildungsraum_course = models.OneToOneField('BildungsraumCourse', on_delete=models.CASCADE, null=True, blank=True, related_name='edx_course')

    class Meta:
        verbose_name = "Open edX course"
        verbose_name_plural = "Open edX courses"

    @display(boolean=True, description="Published")
    def is_published(self):
        return self.bildungsraum_course is not None
    
    def course_url(self):
        return f"https://edx.open-jupyter.gwdg.de/courses/{self.course_id}/"

    def publish_to_bildungsraum(self):
        api = self._get_api_client()
        bildungsraum_course_id = self.course_id
        res = api.publish_course(
            courseId=bildungsraum_course_id,
            title=self.name,
            description=self.short_description,
            uri=self.course_url(),
            startDate=self.start_date,
            endDate=self.end_date,
            cost=self.price_euro,
        )
        # TODO: save the course in the database
        if res == COURSE_CREATED or res == COURSE_UPDATED:
            obj = api.get_course(bildungsraum_course_id)
            # try:
            #     startDate = datetime.strptime(obj['amb']['startDate'], "%Y-%m-%dT%H:%M:%SZ")
            # except:
            #     startDate = None
            # try:
            #     endDate = datetime.strptime(obj['amb']['endDate'], "%Y-%m-%dT%H:%M:%SZ")
            # except:
            #     endDate = None

            # Get the nodeId (=GUID) of the course, so we can construct
            # the URL to the Lernpfadfinder page
            sourceId = "f11c9b55-3b5b-40fe-ba54-284b02783e4f"
            response = api.get_node_by_external_id(sourceId, bildungsraum_course_id)
            nodes = response['_embedded']['nodes']
            nodeId = ""
            for node in nodes:
                if node['externalId'] == bildungsraum_course_id:
                    nodeId = node['id']
                    break

            (brcourse, created) = BildungsraumCourse.objects.update_or_create(
                externalId=bildungsraum_course_id,
                defaults=dict(
                    title=obj['amb']['name'],
                    description=obj['amb']['description'],
                    sourceId=obj['amb']['id'],
                    metadata=obj['amb'],
                    nodeId=nodeId
                    # startDate=startDate,
                    # endDate=endDate,
                )
            )
            self.bildungsraum_course = brcourse
            self.save()

    def unpublish_from_bildungsraum(self):
        api = self._get_api_client()
        res = api.unpublish_course(courseId=self.bildungsraum_course.externalId)
        # delete the course from the database and remove the reference
        self.bildungsraum_course.delete()
        self.bildungsraum_course = None
        self.save()

        return res
    

    def _get_api_client(self) -> PushApi:
        api_base = settings.BILDUNGSRAUM_API_BASE
        client_id = settings.BILDUNGSRAUM_CLIENT_ID
        client_secret = settings.BILDUNGSRAUM_CLIENT_SECRET
        token_url = settings.BILDUNGSRAUM_TOKEN_URL
        return PushApi(api_base, client_id, client_secret, token_url=token_url)

def update_or_create2(model, defaults={}, **kwargs):
    # get if exists, otherwise None
    existing = model.objects.filter(**kwargs).first()
    #update_needed = not existing or any([getattr(existing, k) != v for k, v in defaults.items()])
    if existing:
        update_needed = any([getattr(existing, k) != v for k, v in defaults.items()])
        # if update_needed:
        #     for k, v in defaults.items():
        #         existing_v = getattr(existing, k)
        #         if existing_v != v:
        #             print(f"Updating {k} from {existing_v} to {v}")

        if not update_needed:
            return existing, False, False
    new, created = model.objects.update_or_create(defaults=defaults, **kwargs)
    return new, created, True


def parse_date(date_str: Optional[str]) -> Optional[datetime]:
    if date_str:
        # parse a timezone aware datetime in ISO format
        return datetime.fromisoformat(date_str)
    return None


def refresh_edx_courses(request):
    """ Fetch all courses from the edX API and update the database. """
    api_url = "https://edx.open-jupyter.gwdg.de/api/courses/v1/courses/"
    response = httpx.get(api_url)
    data: EdxCoursesResultDict = response.json()
    for course in data['results']:
        # TODO: handle other currencies...
        price_euro = None  # get_course_price(course['id'])
        print("Handling edx course", course['id'], course['name'])
        new, created, updated = update_or_create2(EdxCourse, course_id=course['id'], defaults=dict(
            name=course['name'],
            number=course['number'],
            org=course['org'],
            start_date=parse_date(course['start']),
            end_date=parse_date(course['end']),
            short_description=course['short_description'] or "",
            price_euro=price_euro,
        ))
        if new.is_published() and (created or updated):
            new.publish_to_bildungsraum()
        
        if created:    
            messages.info(request, f"Created course {new.name}")
        elif updated:
            messages.info(request, f"Updated course {new.name}")

        # EdxCourse.objects.update_or_create(
        #     course_id=course['id'],
        #     defaults=dict(
        #         name=course['name'],
        #         number=course['number'],
        #         org=course['org'],
        #         short_description=course['short_description'] or "",
        #     )
        # )

    # TODO: if a course is published, and was changed, update it in Bildungsraum
    # TODO: if a course is not returned from the API, but is in the database, should we delete it?


def refresh_bildungsraum_courses(request):
    api = PushApi(
        settings.BILDUNGSRAUM_API_BASE,
        settings.BILDUNGSRAUM_CLIENT_ID,
        settings.BILDUNGSRAUM_CLIENT_SECRET,
        settings.BILDUNGSRAUM_TOKEN_URL
    )

    nodes = api.get_nodes('OpenJupyter')
    for node in nodes:
        obj, created, updated = update_or_create2(
            BildungsraumCourse,
            externalId=node.externalId,
            defaults={
                'title': node.title,
                'description': node.description,
                'sourceId': node.sourceId,
                'metadata': node.metadata,
                'nodeId': node.id,
            }
        )

        # Check if a course exists in the edX database:
        edx = EdxCourse.objects.filter(course_id=node.externalId).first()
        if edx:
            if edx.bildungsraum_course != obj:
                edx.bildungsraum_course = obj
                edx.save()
                messages.info(request, f"Linked course {obj.title} to edX course")
        
        if created:
            messages.info(request, f"Found new course {obj.title}")
        elif updated:
            messages.info(request, f"Updated course {obj.title}")


def get_course_price(course_id: str) -> Optional[float]:
    print("get_course_price", course_id)
    api_url = f"https://edx.open-jupyter.gwdg.de/api/commerce/v1/courses/{course_id}/"
    print(api_url)
    response = httpx.get(api_url)
    print(response.status_code)
    if response.status_code == 404:
        return None

    # Example response:
    # {"id":"course-v1:OpenJupyter+DS101+SoSe2023","name":"Introduction to Data Science","verification_deadline":null,"modes":[{"name":"honor","currency":"eur","price":19,"sku":null,"bulk_sku":null,"expires":null}]}
    print(response.text)
    data = response.json()
    try:
        return data['modes'][0]['price']
    except (KeyError, IndexError):
        return None


# Example results from the edX API:
# {"results":[{"blocks_url":"https://edx.open-jupyter.gwdg.de/api/courses/v2/blocks/?course_id=course-v1%3AGeorg-August-UniversityOfGoettingen%2BITCC101%2B2022_T1","effort":null,"end":null,"enrollment_start":null,"enrollment_end":null,"id":"course-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1","media":{"banner_image":{"uri":"/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg","uri_absolute":"https://edx.open-jupyter.gwdg.de/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg"},"course_image":{"uri":"/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg"},"course_video":{"uri":null},"image":{"raw":"https://edx.open-jupyter.gwdg.de/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg","small":"https://edx.open-jupyter.gwdg.de/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg","large":"https://edx.open-jupyter.gwdg.de/asset-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1+type@asset+block@images_course_image.jpg"}},"name":"Introduction To Course Creation","number":"ITCC101","org":"Georg-August-UniversityOfGoettingen","short_description":null,"start":"2030-01-01T00:00:00Z","start_display":null,"start_type":"empty","pacing":"instructor","mobile_available":false,"hidden":false,"invitation_only":false,"course_id":"course-v1:Georg-August-UniversityOfGoettingen+ITCC101+2022_T1"},{"blocks_url":"https://edx.open-jupyter.gwdg.de/api/courses/v2/blocks/?course_id=course-v1%3Aimportexport%2BCourseNumber%2Bcourserun2","effort":null,"end":null,"enrollment_start":null,"enrollment_end":null,"id":"course-v1:importexport+CourseNumber+courserun2","media":{"banner_image":{"uri":"/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg","uri_absolute":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg"},"course_image":{"uri":"/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg"},"course_video":{"uri":null},"image":{"raw":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg","small":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg","large":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+courserun2+type@asset+block@images_course_image.jpg"}},"name":"Demonstration Course","number":"CourseNumber","org":"importexport","short_description":null,"start":"2013-02-05T05:00:00Z","start_display":"Feb. 5, 2013","start_type":"timestamp","pacing":"instructor","mobile_available":false,"hidden":false,"invitation_only":false,"course_id":"course-v1:importexport+CourseNumber+courserun2"},{"blocks_url":"https://edx.open-jupyter.gwdg.de/api/courses/v2/blocks/?course_id=course-v1%3Aimportexport%2BCourseNumber%2BRun1","effort":null,"end":null,"enrollment_start":null,"enrollment_end":null,"id":"course-v1:importexport+CourseNumber+Run1","media":{"banner_image":{"uri":"/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg","uri_absolute":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg"},"course_image":{"uri":"/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg"},"course_video":{"uri":null},"image":{"raw":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg","small":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg","large":"https://edx.open-jupyter.gwdg.de/asset-v1:importexport+CourseNumber+Run1+type@asset+block@images_course_image.jpg"}},"name":"hitchhiker's guide to the datavers","number":"CourseNumber","org":"importexport","short_description":"","start":"2030-01-01T00:00:00Z","start_display":null,"start_type":"empty","pacing":"instructor","mobile_available":false,"hidden":false,"invitation_only":false,"course_id":"course-v1:importexport+CourseNumber+Run1"},{"blocks_url":"https://edx.open-jupyter.gwdg.de/api/courses/v2/blocks/?course_id=course-v1%3AOpenJupyter%2BDS101%2BSoSe2023","effort":null,"end":null,"enrollment_start":null,"enrollment_end":null,"id":"course-v1:OpenJupyter+DS101+SoSe2023","media":{"banner_image":{"uri":"/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@images_course_image.jpg","uri_absolute":"https://edx.open-jupyter.gwdg.de/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@images_course_image.jpg"},"course_image":{"uri":"/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@CaptainMuon_a_group_of_students_working_together_on_a_data_scie_ffc5fdb7-7022-4e4c-a17c-abb7f44de615.png"},"course_video":{"uri":null},"image":{"raw":"https://edx.open-jupyter.gwdg.de/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@CaptainMuon_a_group_of_students_working_together_on_a_data_scie_ffc5fdb7-7022-4e4c-a17c-abb7f44de615.png","small":"https://edx.open-jupyter.gwdg.de/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@CaptainMuon_a_group_of_students_working_together_on_a_data_scie_ffc5fdb7-7022-4e4c-a17c-abb7f44de615.png","large":"https://edx.open-jupyter.gwdg.de/asset-v1:OpenJupyter+DS101+SoSe2023+type@asset+block@CaptainMuon_a_group_of_students_working_together_on_a_data_scie_ffc5fdb7-7022-4e4c-a17c-abb7f44de615.png"}},"name":"Introduction to Data Science","number":"DS101","org":"OpenJupyter","short_description":"An example course to showcase some features of open edX.","start":"2023-06-01T00:00:00Z","start_display":"June 1, 2023","start_type":"timestamp","pacing":"instructor","mobile_available":false,"hidden":false,"invitation_only":false,"course_id":"course-v1:OpenJupyter+DS101+SoSe2023"}],"pagination":{"next":null,"previous":null,"count":4,"num_pages":1}}

from typing import TypedDict, Optional, NotRequired

class EdxCoursesResultDict(TypedDict):
    results: list[EdxCourseDict]

class EdxCourseDict(TypedDict):
    blocks_url: str
    effort: Optional[str]
    end: Optional[str]
    enrollment_start: Optional[str]
    enrollment_end: Optional[str]
    id: str
    media: dict
    name: str
    number: str
    org: str
    short_description: Optional[str]
    start: str
    start_display: Optional[str]
    start_type: str
    pacing: str
    mobile_available: bool
    hidden: bool
    invitation_only: bool
    course_id: str

class BildungsraumCourse(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    nodeId = models.CharField(max_length=100)
    externalId = models.CharField(max_length=100)
    sourceId = models.CharField(max_length=100)
    metadata = models.JSONField()
    class Meta:
        verbose_name = "MeinBildungsraum course"
        verbose_name_plural = "MeinBildungsraum courses"
    def __str__(self):
        return self.title
    
    @display(boolean=True, description="Exists locally")
    def exists_locally(self):
        # If the EdxCourse does not exist, there is no edx_course
        # attribute on the object, so accessing it would raise an
        # Exception. Instead, we use getattr.
        return getattr(self, 'edx_course', None) is not None