
import json
from courses.types import *

EXAMPLE_NODE = """{
    "id": "bb0505b4-064c-4eb8-94d6-748a3e2d4fa6",
    "title": "Data Scientist Associate",
    "description": "This is an example course to test the OpenJupyter Platform. There is no real content here. This is a test.",
    "externalId": "course-v1:import-export+CourseNumber+1000000000000000000000000000000000000000",
    "sourceId": "f11c9b55-3b5b-40fe-ba54-284b02783e4f",
    "metadata": {
        "Amb": {
        "id": "https://nbp/OpenJupyter/course-v1:import-export+CourseNumber+1000000000000000000000000000000000000000",
        "name": "Data Scientist Associate",
        "type": [
            "LearningResource",
            "Course"
        ],
        "@context": [
            "https://w3id.org/kim/amb/context.jsonld",
            "https://schema.org",
            {
            "@language": "de"
            }
        ],
        "publisher": [
            {
            "name": "OpenJupyter",
            "type": "Organization"
            }
        ],
        "description": "This is an example course to test the OpenJupyter Platform. There is no real content here. This is a test.",
        "hasCourseInstance": {
            "type": "CourseInstance",
            "location": {
            "geo": {
                "type": "GeoCoordinates",
                "latitude": "53.5621820995914",
                "longitude": "9.965724480663814"
            },
            "type": "Place",
            "address": "Schanzenstraße 75-77, 20357 Hamburg"
            },
            "startDate": "2030-01-01T00:00:00+00:00"
        },
        "isAccessibleForFree": true
        },
        "Url": "https://www.beispiel.de/kurse/englisch_lernen",
        "Organization": "OpenJupyter"
    },
    "class": "LearningOpportunity",
    "min": 0,
    "max": null,
    "_embedded": null,
    "_links": {
        "self": {
        "href": "https://dam.demo.meinbildungsraum.de/datenraum/api/core/nodes/bb0505b4-064c-4eb8-94d6-748a3e2d4fa6"
        },
        "headNodes": null,
        "tailNodes": null,
        "childNodes": null
    }
}"""

EXAMPLE_NODES_RESPONSE = """{
  "total": 5,
  "offset": 0,
  "limit": 10,
  "_embedded": {
    "nodes": []
  },
  "_links": {
    "self": null,
    "next": null
  }
}"""

def test_node_type():
    node = Node.model_validate_json(EXAMPLE_NODE)
    assert node.id == "bb0505b4-064c-4eb8-94d6-748a3e2d4fa6"

def test_paginated_response():
    pr = PaginatedResponse[NodeList].model_validate_json(EXAMPLE_NODES_RESPONSE)
    assert pr.total == 5
    assert pr.offset == 0
    assert pr.limit == 10
    assert pr.embedded.nodes == []

def test_paginated_response_with_node():
    data = json.loads(EXAMPLE_NODES_RESPONSE)
    data['_embedded']['nodes'].append(json.loads(EXAMPLE_NODE))
    pr = PaginatedResponse[NodeList].model_validate(data)
    assert len(pr.embedded.nodes) == 1

def test_parse_api_core_nodes():
    with open('tests/examples/api-core-nodes.json') as f:
        data = json.load(f)
    pr = PaginatedResponse[NodeList].model_validate(data)
    